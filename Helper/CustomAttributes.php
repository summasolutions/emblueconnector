<?php

namespace Emblue\Connector\Helper;

class CustomAttributes
{
    const CONFIG_PATH_PAYMENTS_MAPPING = 'emblue_general/attributes_mapping/payments';
    const CONFIG_PATH_INTEREST_RATE_MAPPING = 'emblue_general/attributes_mapping/interest_rate';

    /** @var \Magento\Framework\App\Config\ScopeConfigInterface */
    private $scopeConfig;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    public function getPaymentsAttribute()
    {
        return $this->scopeConfig->getValue(self::CONFIG_PATH_PAYMENTS_MAPPING);
    }

    public function getInterestRateAttribute()
    {
        return $this->scopeConfig->getValue(self::CONFIG_PATH_INTEREST_RATE_MAPPING);
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return mixed|null
     */
    public function getPaymentsForProduct($product)
    {
        $attributeCode = $this->getPaymentsAttribute();
        if (!empty($attributeCode)) {
            if (null == !$product->getData($attributeCode)) {
                return $product->getData($attributeCode);
            }
        }

        return '';
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return mixed|null
     */
    public function getInterestRateForProduct($product)
    {
        $attributeCode = $this->getInterestRateAttribute();
        if (!empty($attributeCode)) {
            if (null == !$product->getData($attributeCode)) {
                return $product->getData($attributeCode);
            }
        }

        return '';
    }
}
