<?php
/**
 * Class ProductRepository
 *
 * @author   Facundo Capua <facundocapua@gmail.com>
 * @license  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Emblue\Connector\Model;

use Emblue\Connector\Api\CustomerRepositoryInterface;

class CustomerRepository implements CustomerRepositoryInterface
{
    /** @var \Magento\Customer\Api\CustomerRepositoryInterface */
    private $customerRepository;

    /** @var \Magento\Framework\Api\SearchCriteriaBuilder */
    private $searchCriteriaBuilder;

    /** @var \Magento\Framework\Api\FilterBuilder */
    private $filterBuilder;

    public function __construct(
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Api\FilterBuilder $filterBuilder
    ) {
        $this->customerRepository = $customerRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
    }

    /** @inheritdoc */
    public function getCustomers($dateFrom = NULL, $curPage = 1, $pageSize = 1000, $onlyNew = false)
    {
        $fieldName = $onlyNew ? 'created_at' : 'updated_at';
        $this->searchCriteriaBuilder->addFilter($fieldName, $dateFrom, 'gteq');

        $this->searchCriteriaBuilder->setCurrentPage($curPage);
        $this->searchCriteriaBuilder->setPageSize($pageSize);

        $searchCriteria = $this->searchCriteriaBuilder->create();

        return $this->customerRepository->getList($searchCriteria);
    }
}
