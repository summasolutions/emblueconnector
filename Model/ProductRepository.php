<?php
/**
 * Class ProductRepository
 *
 * @author   Facundo Capua <facundocapua@gmail.com>
 * @license  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Emblue\Connector\Model;

use Emblue\Connector\Api\CatalogRepositoryInterface;
use Magento\Catalog\Model\Product\Attribute\Source\Status as ProductStatus;

class ProductRepository implements CatalogRepositoryInterface
{
    /** @var \Magento\Catalog\Api\ProductRepositoryInterface */
    private $productRepository;

    /** @var \Magento\Framework\Api\SearchCriteriaBuilder */
    private $searchCriteriaBuilder;

    /** @var \Magento\Framework\Api\FilterBuilder */
    private $filterBuilder;

    /** @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory */
    private $productCollectionFactory;

    /** @var \Magento\Framework\App\ProductMetadataInterface */
    private $productMetadata;

    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Framework\App\ProductMetadataInterface $productMetadata
    ) {
        $this->productRepository = $productRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->productMetadata = $productMetadata;
    }

    /** @inheritdoc */
    public function getProducts($productName = '', $curPage = 1, $pageSize = 100)
    {
        if (version_compare($this->productMetadata->getVersion(), '2.3.0') >= 0) {
            $collection = $this->getRepositoryCollection($productName, $curPage, $pageSize);
        } else { //This is for backward compatibility
            $collection = $this->getResourceCollection($productName, $curPage, $pageSize);
        }

        return $collection;
    }

    /** @inheritdoc */
    public function getRelatedProducts($productId)
    {
        $product = $this->productRepository->getById($productId);

        $collection = $product->getRelatedProductCollection();
        $collection->addAttributeToSelect('*');
        $collection->addMediaGalleryData();

        return $collection;
    }

    /**
     * Retrieves a product collection from its repository
     *
     * @param string $productName
     * @param int $curPage
     * @param int $pageSize
     * @return \Magento\Catalog\Api\Data\ProductSearchResultsInterface
     */
    private function getRepositoryCollection($productName = '', $curPage = 1, $pageSize = 100)
    {
        if (!empty($productName)) {
            $filters = [];
            $filters[] = $this->filterBuilder->setField('sku')
                ->setValue("%{$productName}%")
                ->setConditionType('like')
                ->create();
            $filters[] = $this->filterBuilder->setField('name')
                ->setValue("%{$productName}%")
                ->setConditionType('like')
                ->create();
            $this->searchCriteriaBuilder->addFilters($filters);
        }

        $this->searchCriteriaBuilder->addFilter('status', ProductStatus::STATUS_DISABLED, 'neq');

        $this->searchCriteriaBuilder->setCurrentPage($curPage);
        $this->searchCriteriaBuilder->setPageSize($pageSize);

        $searchCriteria = $this->searchCriteriaBuilder->create();

        $collection = $this->productRepository->getList($searchCriteria);

        return $collection;
    }

    /**
     * Generates a product collection
     *
     * @param string $productName
     * @param int $curPage
     * @param int $pageSize
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    private function getResourceCollection($productName = '', $curPage = 1, $pageSize = 100)
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
        $collection = $this->productCollectionFactory->create();
        if (!empty($productName)) {
            $collection->addAttributeToFilter([
                [
                    'attribute' => 'sku',
                    'like' => "%{$productName}%"
                ],
                [
                    'attribute' => 'name',
                    'like' => "%{$productName}%"
                ]
            ]);
        }

        $collection->addAttributeToFilter('status', ['neq' => ProductStatus::STATUS_DISABLED]);
        $collection->setPage($curPage, $pageSize);
        $collection->addAttributeToSelect('*');
        $collection->addMediaGalleryData();

        return $collection;
    }
}
