<?php

namespace Emblue\Connector\Model;

use Emblue\Connector\Api\SubscriberRepositoryInterface;

class SubscriberRepository implements SubscriberRepositoryInterface
{
    /** @var \Magento\Newsletter\Model\ResourceModel\Subscriber\CollectionFactory  */
    private $subscriberCollectionFactory;

    public function __construct(
        \Magento\Newsletter\Model\ResourceModel\Subscriber\CollectionFactory $subscriberCollectionFactory
    ) {
        $this->subscriberCollectionFactory = $subscriberCollectionFactory;
    }

    /** @inheritdoc */
    public function getSubscribers($subscriberStatus = 1, $fromSubscriberId = 1, $curPage = 1, $pageSize = 100)
    {
        $collection = $this->subscriberCollectionFactory->create();

        $collection->addFilter('subscriber_status', $subscriberStatus);

        $collection->addFieldToFilter('subscriber_id', ['gteq' => $fromSubscriberId]);

        $collection->setCurPage($curPage);
        $collection->setPageSize($pageSize);

        $collection->load();

        return $collection;
    }
}
