<?php
/**
 * Class Customer
 *
 * @author   Facundo Capua <facundocapua@gmail.com>
 * @license  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Emblue\Connector\Model\Data;

class Customer extends \Magento\Customer\Model\Data\Customer
{
    public function getGender()
    {
        return 'Female';
    }
}
