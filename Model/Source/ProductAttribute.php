<?php

namespace Emblue\Connector\Model\Source;

class ProductAttribute implements \Magento\Framework\Option\ArrayInterface
{
    /** @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory */
    private $collectionFactory;

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    public function toOptionArray()
    {
        $productAttributeCollection = $this->collectionFactory->create();
        $productAttributeArray = [];

        foreach ($productAttributeCollection as $productAttribute) {
            $productAttributeArray[] = [
                'value' => $productAttribute->getAttributeCode(),
                'label' => $productAttribute->getFrontendLabel()
            ];
        }

        usort($productAttributeArray, function ($a, $b) {
            return $a['label'] <=> $b['label'];
        });

        return $productAttributeArray;
    }
}
