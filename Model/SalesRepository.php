<?php

namespace Emblue\Connector\Model;

use Emblue\Connector\Api\SalesRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Sales\Api\OrderRepositoryInterface;

class SalesRepository implements SalesRepositoryInterface
{
    /** @var \Magento\Customer\Api\CustomerRepositoryInterface */
    private $quoteRepository;

    /** @var OrderRepositoryInterface */
    private $orderRepository;

    /** @var ProductRepositoryInterface */
    private $productRepository;

    /** @var \Magento\Framework\Api\SearchCriteriaBuilder */
    private $searchCriteriaBuilder;

    /** @var \Magento\Framework\Api\FilterBuilder */
    private $filterBuilder;

    public function __construct(
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Api\FilterBuilder $filterBuilder
    ) {
        $this->quoteRepository = $quoteRepository;
        $this->orderRepository = $orderRepository;
        $this->productRepository = $productRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
    }

    /** @inheritdoc */
    public function getCarts(
        $customerEmail = '',
        $curPage = 1,
        $pageSize = 100,
        $sinceCreatedAt = '',
        $toCreatedAt = '',
        $sinceUpdatedAt = '',
        $toUpdatedAt = ''
    ) {
        if (!empty($customerEmail)) {
            $this->searchCriteriaBuilder->addFilter('customer_email', $customerEmail);
        }

        if (!empty($sinceCreatedAt)) {
            $this->searchCriteriaBuilder->addFilter('created_at', $sinceCreatedAt, 'gteq');
        }

        if (!empty($toCreatedAt)) {
            $this->searchCriteriaBuilder->addFilter('created_at', $toCreatedAt, 'lteq');
        }

        if (!empty($sinceUpdatedAt)) {
            $this->searchCriteriaBuilder->addFilter('updated_at', $sinceUpdatedAt, 'gteq');
        }

        if (!empty($toUpdatedAt)) {
            $this->searchCriteriaBuilder->addFilter('updated_at', $toUpdatedAt, 'lteq');
        }

        $this->searchCriteriaBuilder->addFilter('is_active', true);
        $this->searchCriteriaBuilder->addFilter('customer_email', '', 'notnull');

        $this->searchCriteriaBuilder->setCurrentPage($curPage);
        $this->searchCriteriaBuilder->setPageSize($pageSize);

        $searchCriteria = $this->searchCriteriaBuilder->create();

        $collection = $this->quoteRepository->getList($searchCriteria);

        foreach ($collection->getItems() as $quote) {
            foreach ($quote->getItems() as $quoteItem) {
                $product = $this->productRepository->getById($quoteItem->getProduct()->getId());
                $quoteItem->setProduct($product);
            }
        }

        return $collection;
    }

    /** @inheritdoc */
    public function getOrders(
        $curPage = 1,
        $pageSize = 100,
        $customerEmail = '',
        $sinceCreatedAt = '',
        $toCreatedAt = '',
        $sinceUpdatedAt = '',
        $toUpdatedAt = '',
        $status = 'pending'
    ) {
        if (!empty($customerEmail)) {
            $this->searchCriteriaBuilder->addFilter('customer_email', $customerEmail);
        }

        if (!empty($sinceCreatedAt)) {
            $this->searchCriteriaBuilder->addFilter('created_at', $sinceCreatedAt, 'gteq');
        }

        if (!empty($toCreatedAt)) {
            $this->searchCriteriaBuilder->addFilter('created_at', $toCreatedAt, 'lteq');
        }

        if (!empty($sinceUpdatedAt)) {
            $this->searchCriteriaBuilder->addFilter('updated_at', $sinceUpdatedAt, 'gteq');
        }

        if (!empty($toUpdatedAt)) {
            $this->searchCriteriaBuilder->addFilter('updated_at', $toUpdatedAt, 'lteq');
        }

        if (!empty($status)) {
            $this->searchCriteriaBuilder->addFilter('status', $status);
        }

        $this->searchCriteriaBuilder->setCurrentPage($curPage);
        $this->searchCriteriaBuilder->setPageSize($pageSize);

        $searchCriteria = $this->searchCriteriaBuilder->create();

        return $this->orderRepository->getList($searchCriteria);
    }
}
