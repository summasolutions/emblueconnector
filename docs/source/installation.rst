############
Installation
############


************
Requirements
************

* Magento 2 Installation

+------------------------+------------+
| Edition                | Version(s) |
+========================+============+
| Open Source Edition    |  2.3.0     |
+------------------------+------------+
| Commerce Edition       |  2.1.9     |
+------------------------+------------+
| Cloud Edition          |  2.3.0     |
+------------------------+------------+

*********************
Composer Installation
*********************

Install the module by running::

    composer require emblue_marketing_cloud/magento2-connector


************
Module Setup
************

Once the module in intalled then it needs to be enabled by running::

    bin/magento module:enable Emblue_Connector