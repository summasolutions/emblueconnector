
#############
API
#############


*********************
Connecting to the API
*********************

In order to connect to the API, first create a new integration, see :doc:`/configuration`

The generated credentials must be used to make authenticate all request with `OAuth 1 <http://docs.akana.com/cm/api_oauth/aaref/Ref_OAuth_AuthorizationHeader_10a.htm>`_

An example using `Postman <https://www.getpostman.com/>`_ would be

.. image:: _images/postman.jpg


*********
Endpoints
*********

========
Products
========

+--------------------------------------+--------+-----------------------------------------+
| Method                               | HTTP   | Description                             |
+======================================+========+=========================================+
| :ref:`/V1/emblue/products`           |  GET   |  Gets the list of products              |
+--------------------------------------+--------+-----------------------------------------+
| :ref:`/V1/emblue/products/related`   |  GET   |  Gets the list of related products      |
+--------------------------------------+--------+-----------------------------------------+

.. _/V1/emblue/products:

-------------------
/V1/emblue/products
-------------------

^^^^^^^
Request
^^^^^^^

+------------------+------------+---------+-----------------------------------------+
| Parameter        | Type       | Default | Description                             |
+==================+============+=========+=========================================+
| productName      |  String    | null    | Query to filter by product name or sku  +
+------------------+------------+---------+-----------------------------------------+
| curPage          |  Integer   | 1       | Page number                             +
+------------------+------------+---------+-----------------------------------------+
| pageSize         |  Integer   | 100     | Number of items per page                +
+------------------+------------+---------+-----------------------------------------+


^^^^^^^^
Response
^^^^^^^^
This is a sample response::

    {
        "items": [
            {
                "id":1,
                "sku":"24-MB01",
                "name":"Joust Duffle Bag",
                "price":34,
                "status":1,
                "final_price":34,
                "product_url":"https:\/\/ee219.magento.test\/index.php\/joust-duffle-bag.html",
                "url_key":"joust-duffle-bag",
                "website_ids":[
                    1
                ],
                "description":"<p>The sporty Joust Duffle Bag can't be beat - not in the gym, not on the luggage carousel, not anywhere. Big enough to haul a basketball or soccer ball and some sneakers with plenty of room to spare, it's ideal for athletes with places to go.<p>\r\n<ul>\r\n<li>Dual top handles.<\/li>\r\n<li>Adjustable shoulder strap.<\/li>\r\n<li>Full-length zipper.<\/li>\r\n<li>L 29\" x W 13\" x H 11\".<\/li>\r\n<\/ul>",
                "type_id": "simple",
                "custom_attributes":[
                    {
                        "attribute_code":"description",
                        "value":"<p>The sporty Joust Duffle Bag can't be beat - not in the gym, not on the luggage carousel, not anywhere. Big enough to haul a basketball or soccer ball and some sneakers with plenty of room to spare, it's ideal for athletes with places to go.<p>\r\n<ul>\r\n<li>Dual top handles.<\/li>\r\n<li>Adjustable shoulder strap.<\/li>\r\n<li>Full-length zipper.<\/li>\r\n<li>L 29\" x W 13\" x H 11\".<\/li>\r\n<\/ul>"
                    },
                    {
                        "attribute_code":"image",
                        "value":"\/m\/b\/mb01-blue-0.jpg"
                    },
                    {
                        "attribute_code":"small_image",
                        "value":"\/m\/b\/mb01-blue-0.jpg"
                    },
                    {
                        "attribute_code":"thumbnail",
                        "value":"\/m\/b\/mb01-blue-0.jpg"
                    },
                    {
                        "attribute_code":"options_container",
                        "value":"container1"
                    },
                    {
                        "attribute_code":"required_options",
                        "value":"0"
                    },
                    {
                        "attribute_code":"has_options",
                        "value":"0"
                    },
                    {
                        "attribute_code":"url_key",
                        "value":"joust-duffle-bag"
                    },
                    {
                        "attribute_code":"msrp_display_actual_price_type",
                        "value":"0"
                    },
                    {
                        "attribute_code":"tax_class_id",
                        "value":"0"
                    },
                    {
                        "attribute_code":"gift_message_available",
                        "value":"0"
                    },
                    {
                        "attribute_code":"gift_wrapping_available",
                        "value":"0"
                    },
                    {
                        "attribute_code":"is_returnable",
                        "value":"0"
                    },
                    {
                        "attribute_code":"eco_collection",
                        "value":"0"
                    },
                    {
                        "attribute_code":"performance_fabric",
                        "value":"0"
                    },
                    {
                        "attribute_code":"erin_recommends",
                        "value":"0"
                    },
                    {
                        "attribute_code":"new",
                        "value":"0"
                    },
                    {
                        "attribute_code":"sale",
                        "value":"0"
                    },
                    {
                        "attribute_code":"payments",
                        "value":"12"
                    },
                    {
                        "attribute_code":"interest_rate",
                        "value":"1.5"
                    },
                    {
                        "attribute_code":"best_price",
                        "value":"123.0000"
                    }
                ]
            }
        ]
    }


.. _/V1/emblue/products/related:

-----------------------------
/V1/emblue/products/related
-----------------------------

^^^^^^^
Request
^^^^^^^

+------------------+------------+---------+-----------------------------------------+
| Parameter        | Type       | Default | Description                             |
+==================+============+=========+=========================================+
| productId _*_    |  String    | null    | Product ID to get the related  products +
+------------------+------------+---------+-----------------------------------------+

^^^^^^^^
Response
^^^^^^^^
This is a sample response::

    {
        "items": [
            {
                "id":2,
                "sku":"24-MB04",
                "name":"Strive Shoulder Pack",
                "price":32,
                "status":1,
                "final_price":32,
                "product_url":"https:\/\/ee219.magento.test\/index.php\/strive-shoulder-pack.html",
                "special_price":32,
                "special_from_date":"2019-04-25 09:47:56",
                "url_key":"strive-shoulder-pack",
                "website_ids":[
                    1
                ],
                "description":"<p>Convenience is next to nothing when your day is crammed with action. So whether you're heading to class, gym, or the unbeaten path, make sure you've got your Strive Shoulder Pack stuffed with all your essentials, and extras as well.<\/p>\r\n<ul>\r\n<li>Zippered main compartment.<\/li>\r\n<li>Front zippered pocket.<\/li>\r\n<li>Side mesh pocket.<\/li>\r\n<li>Cell phone pocket on strap.<\/li>\r\n<li>Adjustable shoulder strap and top carry handle.<\/li>\r\n<\/ul>",
                "media_gallery_images":{
                    "items":[
                        {
                        "url":"https:\/\/ee219.magento.test\/media\/catalog\/product\/m\/b\/mb04-black-0.jpg"
                        },
                        {
                        "url":"https:\/\/ee219.magento.test\/media\/catalog\/product\/m\/b\/mb04-black-0_alt1.jpg"
                        }
                    ]
                },
                "type_id": "simple",
                "custom_attributes":[
                    {
                        "attribute_code":"description",
                        "value":"<p>Convenience is next to nothing when your day is crammed with action. So whether you're heading to class, gym, or the unbeaten path, make sure you've got your Strive Shoulder Pack stuffed with all your essentials, and extras as well.<\/p>\r\n<ul>\r\n<li>Zippered main compartment.<\/li>\r\n<li>Front zippered pocket.<\/li>\r\n<li>Side mesh pocket.<\/li>\r\n<li>Cell phone pocket on strap.<\/li>\r\n<li>Adjustable shoulder strap and top carry handle.<\/li>\r\n<\/ul>"
                    },
                    {
                        "attribute_code":"special_price",
                        "value":"32.0000"
                    },
                    {
                        "attribute_code":"special_from_date",
                        "value":"2019-04-25 09:47:56"
                    },
                    {
                        "attribute_code":"image",
                        "value":"\/m\/b\/mb04-black-0.jpg"
                    },
                    {
                        "attribute_code":"small_image",
                        "value":"\/m\/b\/mb04-black-0.jpg"
                    },
                    {
                        "attribute_code":"thumbnail",
                        "value":"\/m\/b\/mb04-black-0.jpg"
                    },
                    {
                        "attribute_code":"news_from_date",
                        "value":"2019-04-25 09:47:56"
                    },
                    {
                        "attribute_code":"custom_design_from",
                        "value":"2019-04-25 09:47:56"
                    },
                    {
                        "attribute_code":"options_container",
                        "value":"container2"
                    },
                    {
                        "attribute_code":"required_options",
                        "value":"0"
                    },
                    {
                        "attribute_code":"has_options",
                        "value":"0"
                    },
                    {
                        "attribute_code":"url_key",
                        "value":"strive-shoulder-pack"
                    },
                    {
                        "attribute_code":"msrp_display_actual_price_type",
                        "value":"0"
                    },
                    {
                        "attribute_code":"tax_class_id",
                        "value":"2"
                    },
                    {
                        "attribute_code":"gift_message_available",
                        "value":"0"
                    },
                    {
                        "attribute_code":"gift_wrapping_available",
                        "value":"0"
                    },
                    {
                        "attribute_code":"is_returnable",
                        "value":"2"
                    },
                    {
                        "attribute_code":"activity",
                        "value":"20,30,32,27"
                    },
                    {
                        "attribute_code":"style_bags",
                        "value":"36,38,39"
                    },
                    {
                        "attribute_code":"material",
                        "value":"41,42,45,47"
                    },
                    {
                        "attribute_code":"strap_bags",
                        "value":"70,71,74,75,76"
                    },
                    {
                        "attribute_code":"features_bags",
                        "value":"81,83,84,87"
                    },
                    {
                        "attribute_code":"eco_collection",
                        "value":"0"
                    },
                    {
                        "attribute_code":"performance_fabric",
                        "value":"0"
                    },
                    {
                        "attribute_code":"erin_recommends",
                        "value":"1"
                    },
                    {
                        "attribute_code":"new",
                        "value":"0"
                    },
                    {
                        "attribute_code":"sale",
                        "value":"1"
                    },
                    {
                        "attribute_code":"payments",
                        "value":"6"
                    },
                    {
                        "attribute_code":"interest_rate",
                        "value":"1.1"
                    }
                ]
            },
            {
                "id":3,
                "sku":"24-MB03",
                "name":"Crown Summit Backpack",
                "price":38,
                "status":1,
                "final_price":38,
                "product_url":"https:\/\/ee219.magento.test\/index.php\/crown-summit-backpack.html",
                "url_key":"crown-summit-backpack",
                "website_ids":[
                    1
                ],
                "description":"<p>The Crown Summit Backpack is equally at home in a gym locker, study cube or a pup tent, so be sure yours is packed with books, a bag lunch, water bottles, yoga block, laptop, or whatever else you want in hand. Rugged enough for day hikes and camping trips, it has two large zippered compartments and padded, adjustable shoulder straps.<\/p>\n<ul>\n<li>Top handle.<\/li>\n<li>Grommet holes.<\/li>\n<li>Two-way zippers.<\/li>\n<li>H 20\" x W 14\" x D 12\".<\/li>\n<li>Weight: 2 lbs, 8 oz. Volume: 29 L.<\/li>\n<ul>",
                "media_gallery_images":{
                    "items":[
                        {
                        "url":"https:\/\/ee219.magento.test\/media\/catalog\/product\/m\/b\/mb03-black-0.jpg"
                        },
                        {
                        "url":"https:\/\/ee219.magento.test\/media\/catalog\/product\/m\/b\/mb03-black-0_alt1.jpg"
                        }
                    ]
                },
                "type_id": "simple",
                "custom_attributes":[
                    {
                        "attribute_code":"description",
                        "value":"<p>The Crown Summit Backpack is equally at home in a gym locker, study cube or a pup tent, so be sure yours is packed with books, a bag lunch, water bottles, yoga block, laptop, or whatever else you want in hand. Rugged enough for day hikes and camping trips, it has two large zippered compartments and padded, adjustable shoulder straps.<\/p>\n<ul>\n<li>Top handle.<\/li>\n<li>Grommet holes.<\/li>\n<li>Two-way zippers.<\/li>\n<li>H 20\" x W 14\" x D 12\".<\/li>\n<li>Weight: 2 lbs, 8 oz. Volume: 29 L.<\/li>\n<ul>"
                    },
                    {
                        "attribute_code":"image",
                        "value":"\/m\/b\/mb03-black-0.jpg"
                    },
                    {
                        "attribute_code":"small_image",
                        "value":"\/m\/b\/mb03-black-0.jpg"
                    },
                    {
                        "attribute_code":"thumbnail",
                        "value":"\/m\/b\/mb03-black-0.jpg"
                    },
                    {
                        "attribute_code":"options_container",
                        "value":"container2"
                    },
                    {
                        "attribute_code":"required_options",
                        "value":"0"
                    },
                    {
                        "attribute_code":"has_options",
                        "value":"0"
                    },
                    {
                        "attribute_code":"url_key",
                        "value":"crown-summit-backpack"
                    },
                    {
                        "attribute_code":"tax_class_id",
                        "value":"2"
                    },
                    {
                        "attribute_code":"is_returnable",
                        "value":"2"
                    },
                    {
                        "attribute_code":"activity",
                        "value":"20,27,28,29,30,31,32"
                    },
                    {
                        "attribute_code":"style_bags",
                        "value":"33"
                    },
                    {
                        "attribute_code":"material",
                        "value":"46,47"
                    },
                    {
                        "attribute_code":"strap_bags",
                        "value":"70,73,74"
                    },
                    {
                        "attribute_code":"features_bags",
                        "value":"81,83,84,86,87"
                    }
                ]
            },
            {
                "id":4,
                "sku":"24-MB05",
                "name":"Wayfarer Messenger Bag",
                "price":45,
                "status":1,
                "final_price":45,
                "product_url":"https:\/\/ee219.magento.test\/index.php\/wayfarer-messenger-bag.html",
                "url_key":"wayfarer-messenger-bag",
                "website_ids":[
                    1
                ],
                "description":"<p>Perfect for class, work or the gym, the Wayfarer Messenger Bag is packed with pockets. The dual-buckle flap closure reveals an organizational panel, and the roomy main compartment has spaces for your laptop and a change of clothes. An adjustable shoulder strap and easy-grip handle promise easy carrying.<\/p>\n<ul>\n<li>Multiple internal zip pockets.<\/li>\n<li>Made of durable nylon.<\/li>\n<\/ul>",
                "media_gallery_images":{
                    "items":[
                        {
                        "url":"https:\/\/ee219.magento.test\/media\/catalog\/product\/m\/b\/mb05-black-0.jpg"
                        }
                    ]
                },
                "type_id": "simple",
                "custom_attributes":[
                    {
                        "attribute_code":"description",
                        "value":"<p>Perfect for class, work or the gym, the Wayfarer Messenger Bag is packed with pockets. The dual-buckle flap closure reveals an organizational panel, and the roomy main compartment has spaces for your laptop and a change of clothes. An adjustable shoulder strap and easy-grip handle promise easy carrying.<\/p>\n<ul>\n<li>Multiple internal zip pockets.<\/li>\n<li>Made of durable nylon.<\/li>\n<\/ul>"
                    },
                    {
                        "attribute_code":"image",
                        "value":"\/m\/b\/mb05-black-0.jpg"
                    },
                    {
                        "attribute_code":"small_image",
                        "value":"\/m\/b\/mb05-black-0.jpg"
                    },
                    {
                        "attribute_code":"thumbnail",
                        "value":"\/m\/b\/mb05-black-0.jpg"
                    },
                    {
                        "attribute_code":"options_container",
                        "value":"container2"
                    },
                    {
                        "attribute_code":"required_options",
                        "value":"0"
                    },
                    {
                        "attribute_code":"has_options",
                        "value":"0"
                    },
                    {
                        "attribute_code":"url_key",
                        "value":"wayfarer-messenger-bag"
                    },
                    {
                        "attribute_code":"tax_class_id",
                        "value":"2"
                    },
                    {
                        "attribute_code":"is_returnable",
                        "value":"2"
                    },
                    {
                        "attribute_code":"activity",
                        "value":"20,28,31"
                    },
                    {
                        "attribute_code":"style_bags",
                        "value":"36,37"
                    },
                    {
                        "attribute_code":"material",
                        "value":"46,47"
                    },
                    {
                        "attribute_code":"strap_bags",
                        "value":"70,72,73,74,75"
                    },
                    {
                        "attribute_code":"features_bags",
                        "value":"83,84,86,87,88"
                    },
                    {
                        "attribute_code":"new",
                        "value":"1"
                    }
                ]
            }
            ]
    }


=========
Customers
=========

+--------------------------------------+--------+-----------------------------------------+
| Method                               | HTTP   | Description                             |
+======================================+========+=========================================+
| :ref:`/V1/emblue/customers`          |  GET   |  Gets the list of customers             |
+--------------------------------------+--------+-----------------------------------------+


.. _/V1/emblue/customers:

--------------------
/V1/emblue/customers
--------------------

^^^^^^^
Request
^^^^^^^

+------------------+------------+---------+-----------------------------------------+
| Parameter        | Type       | Default | Description                             |
+==================+============+=========+=========================================+
| dateFrom         |  Date      | null    | Format: YYYY-MM-DD HH:MM:SS             +
+------------------+------------+---------+-----------------------------------------+
| onlyNew          |  Boolean   | false   | Only return new customers               +
+------------------+------------+---------+-----------------------------------------+
| curPage          |  Integer   | 1       | Page number                             +
+------------------+------------+---------+-----------------------------------------+
| pageSize         |  Integer   | 100     | Number of items per page                +
+------------------+------------+---------+-----------------------------------------+

^^^^^^^^
Response
^^^^^^^^
This is a sample response::

    {
        "items": [
            {
                "id": 1,
                "group_id": 1,
                "default_billing": "1",
                "default_shipping": "1",
                "created_at": "2019-01-30 18:47:59",
                "updated_at": "2019-02-08 15:54:49",
                "created_in": "Default Store View",
                "dob": "1973-12-15",
                "email": "roni_cost@example.com",
                "firstname": "Veronicaa",
                "lastname": "Costello",
                "gender": 2,
                "store_id": 1,
                "website_id": 1,
                "addresses": [
                    {
                        "id": 1,
                        "customer_id": 1,
                        "region": {
                            "region_code": "MI",
                            "region": "Michigan",
                            "region_id": 33
                        },
                        "region_id": 33,
                        "country_id": "US",
                        "street": [
                            "6146 Honey Bluff Parkway"
                        ],
                        "telephone": "(555) 229-3326",
                        "postcode": "49628-7978",
                        "city": "Calder",
                        "firstname": "Veronica",
                        "lastname": "Costello",
                        "default_shipping": true,
                        "default_billing": true
                    }
                ],
                "disable_auto_group_change": 0
            },
            {
                "id": 2,
                "group_id": 1,
                "default_billing": "0",
                "default_shipping": "0",
                "created_at": "2019-02-08 15:39:34",
                "updated_at": "2019-02-14 14:40:09",
                "created_in": "Default Store View",
                "email": "john@doe.com",
                "firstname": "John",
                "lastname": "Doe",
                "gender": 1,
                "store_id": 1,
                "website_id": 1,
                "addresses": [],
                "disable_auto_group_change": 0
            }
        ],
        "search_criteria": {
            "filter_groups": [
                {
                    "filters": [
                        {
                            "field": "updated_at",
                            "value": "",
                            "condition_type": "gteq"
                        }
                    ]
                }
            ],
            "page_size": 1000,
            "current_page": 1
        },
        "total_count": 2
    }


===========
Subscribers
===========

+--------------------------------------+--------+-----------------------------------------+
| Method                               | HTTP   | Description                             |
+======================================+========+=========================================+
| :ref:`/V1/emblue/subscribers`        |  GET   |  Gets the list of subscribers           |
+--------------------------------------+--------+-----------------------------------------+


.. _/V1/emblue/subscribers:

----------------------
/V1/emblue/subscribers
----------------------

^^^^^^^
Request
^^^^^^^

+------------------+------------+---------+-----------------------------------------+
| Parameter        | Type       | Default | Description                             |
+==================+============+=========+=========================================+
| subscriberStatus |  Integer   | 1       | | 1: Subscribed                         +
|                  |            |         | | 2: Not Activated                      +
|                  |            |         | | 3: Unsubscribed                       +
|                  |            |         | | 4: Unconfirmed                        +
+------------------+------------+---------+-----------------------------------------+
| fromSubscriberId |  Integer   | null    | Starting from this subscriber id        +
+------------------+------------+---------+-----------------------------------------+
| curPage          |  Integer   | 1       | Page number                             +
+------------------+------------+---------+-----------------------------------------+
| pageSize         |  Integer   | 100     | Number of items per page                +
+------------------+------------+---------+-----------------------------------------+

^^^^^^^^
Response
^^^^^^^^
This is a sample response::

    {
        "items": [
            {
                "id": 2,
                "email": "john@doe.com",
                "status": 1,
                "customer_id": 0
            },
            {
                "id": 3,
                "email": "jane.doe@example.com",
                "status": 1,
                "customer_id": 0
            }
        ]
    }


=====
Sales
=====

+--------------------------------------+--------+-----------------------------------------+
| Method                               | HTTP   | Description                             |
+======================================+========+=========================================+
| :ref:`/V1/emblue/carts`              |  GET   |  Gets the list of shopping carts        |
+--------------------------------------+--------+-----------------------------------------+
| :ref:`/V1/emblue/orders`             |  GET   |  Gets the list of orders                |
+--------------------------------------+--------+-----------------------------------------+


.. _/V1/emblue/carts:

----------------
/V1/emblue/carts
----------------

^^^^^^^
Request
^^^^^^^

+------------------+------------+---------+-----------------------------------------+
| Parameter        | Type       | Default | Description                             |
+==================+============+=========+=========================================+
| customerEmail    |  String    | null    | Customer email to filter by             +
+------------------+------------+---------+-----------------------------------------+
| sinceCreatedAt   |  Date      | null    | Format: Format: YYYY-MM-DD HH:MM:SS     +
+------------------+------------+---------+-----------------------------------------+
| toCreatedAt      |  Date      | null    | Format: Format: YYYY-MM-DD HH:MM:SS     +
+------------------+------------+---------+-----------------------------------------+
| sinceUpdatedAt   |  Date      | null    | Format: Format: YYYY-MM-DD HH:MM:SS     +
+------------------+------------+---------+-----------------------------------------+
| toUpdatedAt      |  Date      | null    | Format: Format: YYYY-MM-DD HH:MM:SS     +
+------------------+------------+---------+-----------------------------------------+
| curPage          |  Integer   | 1       | Page number                             +
+------------------+------------+---------+-----------------------------------------+
| pageSize         |  Integer   | 100     | Number of items per page                +
+------------------+------------+---------+-----------------------------------------+

^^^^^^^^
Response
^^^^^^^^
This is a sample response::

    {
        "items": [
            {
                "items": [
                    {
                        "product": {
                            "id": 48,
                            "sku": "240-LV05",
                            "name": "LifeLong Fitness IV",
                            "price": 14,
                            "status": 1,
                            "formatted_price": "<span class=\"price\">$14.00</span>",
                            "final_price": 14,
                            "product_url": "http://emblue.localhost/index.php/lifelong-fitness-iv.html",
                            "url_key": "lifelong-fitness-iv",
                            "website_ids": [
                                1
                            ],
                            "description": "<p>Luma LifeLong Fitness Series is a world recognized, evidence based exercise program designed specifically for individuals focused on staying active their whole lives. If followed regularly, participants will see improved heart rate and blood pressure, increased mobility, reduced joint pain and overall improvement in functional fitness and health.</>\n<ul>\n<li>10 minute warm up.</li>\n<li>30 minutes of mild aerobics.</li>\n<li>20 minutes of strength, stretch and balance.</li>\n<li>Extensive modifications for varying fitness levels.</li>\n</ul>",
                            "media_gallery_images": {
                                "items": [
                                    {
                                        "url": "http://emblue.localhost/pub/media/catalog/product/l/t/lt02.jpg"
                                    }
                                ]
                            }
                        },
                        "item_id": 7,
                        "sku": "240-LV05",
                        "qty": 1,
                        "name": "LifeLong Fitness IV",
                        "price": 14,
                        "product_type": "downloadable",
                        "quote_id": "3"
                    },
                    {
                        "product": {
                            "id": 1402,
                            "sku": "WJ12-S-Blue",
                            "name": "Olivia 1/4 Zip Light Jacket",
                            "price": 77,
                            "status": 1,
                            "formatted_price": "<span class=\"price\">$77.00</span>",
                            "final_price": 77,
                            "product_url": "http://emblue.localhost/index.php/olivia-1-4-zip-light-jacket.html",
                            "url_key": "olivia-1-4-zip-light-jacket",
                            "website_ids": [
                                1
                            ],
                            "description": "<p>Running errands or headed to the gym, you just want to be comfortable. The Olivia Light Jacket promises that, plus a laid-back look. This zip-up is designed with shoulder stripes for an athletic touch, and banded waist and contoured seams for a flattering silhouette.</p>\n<p>&bull; Loose fit.</br>&bull; Reflectivity.</br>&bull; Flat seams.</br>&bull; Machine wash/dry.</p>",
                            "media_gallery_images": {
                                "items": [
                                    {
                                        "url": "http://emblue.localhost/pub/media/catalog/product/w/j/wj12-blue_main_1.jpg"
                                    },
                                    {
                                        "url": "http://emblue.localhost/pub/media/catalog/product/w/j/wj12-blue_alt1_1.jpg"
                                    },
                                    {
                                        "url": "http://emblue.localhost/pub/media/catalog/product/w/j/wj12-blue_back_1.jpg"
                                    }
                                ]
                            }
                        },
                        "item_id": 8,
                        "sku": "WJ12-S-Blue",
                        "qty": 1,
                        "name": "Olivia 1/4 Zip Light Jacket",
                        "price": 77,
                        "product_type": "configurable",
                        "quote_id": "3"
                    }
                ],
                "id": 3,
                "created_at": "2019-02-08 15:39:41",
                "updated_at": "2019-02-15 12:02:29",
                "is_active": true,
                "is_virtual": false,
                "items_count": 2,
                "items_qty": 2,
                "customer": {
                    "id": 2,
                    "group_id": 1,
                    "default_billing": "0",
                    "default_shipping": "0",
                    "created_at": "2019-02-08 15:39:34",
                    "updated_at": "2019-02-14 14:40:09",
                    "created_in": "Default Store View",
                    "email": "john@doe.com",
                    "firstname": "John",
                    "lastname": "Doe",
                    "gender": 1,
                    "store_id": 1,
                    "website_id": 1,
                    "addresses": [],
                    "disable_auto_group_change": 0,
                    "extension_attributes": {
                        "is_subscribed": false
                    }
                },
                "billing_address": {
                    "id": 5,
                    "region": null,
                    "region_id": null,
                    "region_code": null,
                    "country_id": null,
                    "street": [
                        ""
                    ],
                    "telephone": null,
                    "postcode": null,
                    "city": null,
                    "firstname": null,
                    "lastname": null,
                    "customer_id": 2,
                    "email": "john@doe.com",
                    "same_as_billing": 0,
                    "save_in_address_book": 0
                },
                "orig_order_id": 0,
                "currency": {
                    "global_currency_code": "USD",
                    "base_currency_code": "USD",
                    "store_currency_code": "USD",
                    "quote_currency_code": "USD",
                    "store_to_base_rate": 0,
                    "store_to_quote_rate": 0,
                    "base_to_global_rate": 1,
                    "base_to_quote_rate": 1
                },
                "customer_is_guest": false,
                "customer_note_notify": true,
                "customer_tax_class_id": 3,
                "store_id": 1,
                "extension_attributes": {
                    "shipping_assignments": [
                        {
                            "shipping": {
                                "address": {
                                    "id": 6,
                                    "region": null,
                                    "region_id": null,
                                    "region_code": null,
                                    "country_id": null,
                                    "street": [
                                        ""
                                    ],
                                    "telephone": null,
                                    "postcode": null,
                                    "city": null,
                                    "firstname": null,
                                    "lastname": null,
                                    "customer_id": 2,
                                    "email": "john@doe.com",
                                    "same_as_billing": 1,
                                    "save_in_address_book": 0
                                },
                                "method": null
                            },
                            "items": [
                                {
                                    "item_id": 7,
                                    "sku": "240-LV05",
                                    "qty": 1,
                                    "name": "LifeLong Fitness IV",
                                    "price": 14,
                                    "product_type": "downloadable",
                                    "quote_id": "3"
                                },
                                {
                                    "item_id": 8,
                                    "sku": "WJ12-S-Blue",
                                    "qty": 1,
                                    "name": "Olivia 1/4 Zip Light Jacket",
                                    "price": 77,
                                    "product_type": "configurable",
                                    "quote_id": "3"
                                }
                            ]
                        }
                    ]
                }
            }
        ]
    }


.. _/V1/emblue/orders:

-----------------
/V1/emblue/orders
-----------------

^^^^^^^
Request
^^^^^^^

+------------------+------------+---------+-----------------------------------------+
| Parameter        | Type       | Default | Description                             |
+==================+============+=========+=========================================+
| customerEmail    |  String    | null    | Customer email to filter by             +
+------------------+------------+---------+-----------------------------------------+
| sinceCreatedAt   |  Date      | null    | Format: Format: YYYY-MM-DD HH:MM:SS     +
+------------------+------------+---------+-----------------------------------------+
| toCreatedAt      |  Date      | null    | Format: Format: YYYY-MM-DD HH:MM:SS     +
+------------------+------------+---------+-----------------------------------------+
| sinceUpdatedAt   |  Date      | null    | Format: Format: YYYY-MM-DD HH:MM:SS     +
+------------------+------------+---------+-----------------------------------------+
| toUpdatedAt      |  Date      | null    | Format: Format: YYYY-MM-DD HH:MM:SS     +
+------------------+------------+---------+-----------------------------------------+
| status           |  String    | pending | | Possible values are:                  +
|                  |            |         | | pending                               +
|                  |            |         | | pending_payment                       +
|                  |            |         | | processing                            +
|                  |            |         | | payment_review                        +
|                  |            |         | | holded                                +
|                  |            |         | | fraud                                 +
|                  |            |         | | complete                              +
|                  |            |         | | closed                                +
|                  |            |         | | canceled                              +
+------------------+------------+---------+-----------------------------------------+
| curPage          |  Integer   | 1       | Page number                             +
+------------------+------------+---------+-----------------------------------------+
| pageSize         |  Integer   | 100     | Number of items per page                +
+------------------+------------+---------+-----------------------------------------+

^^^^^^^^
Response
^^^^^^^^
This is a sample response::

    {
        "items": [
            {
                "items": [
                    {
                        "product": {
                            "id": 1434,
                            "sku": "WS03",
                            "name": "Iris Workout Top",
                            "price": 0,
                            "status": 1,
                            "formatted_price": "<span class=\"price\">$0.00</span>",
                            "final_price": 0,
                            "product_url": "http://emblue.localhost/index.php/catalog/product/view/id/1434/s/iris-workout-top/",
                            "url_key": "iris-workout-top",
                            "website_ids": [
                                1
                            ],
                            "description": "<p>The Iris Workout Top is sleeveless, body fitting and moisture wicking for a fashionable, functional garment. Rouched textures add style.</p>\n<p>&bull; Pink heather rouched v-neck.<br />&bull; Scoop neckline.<br />&bull; Angled flat seams.<br />&bull; Moisture wicking.<br />&bull; Body skimming.<br />&bull; 83% Polyester / 11% TENCEL&reg; Lyocell / 6% Lycra&reg; Spandex.</p>",
                            "media_gallery_images": {
                                "items": [
                                    {
                                        "url": "http://emblue.localhost/pub/media/catalog/product/w/s/ws03-red_main_1.jpg"
                                    },
                                    {
                                        "url": "http://emblue.localhost/pub/media/catalog/product/w/s/ws03-red_alt1_1.jpg"
                                    },
                                    {
                                        "url": "http://emblue.localhost/pub/media/catalog/product/w/s/ws03-red_back_1.jpg"
                                    }
                                ]
                            }
                        },
                        "amount_refunded": 0,
                        "base_amount_refunded": 0,
                        "base_discount_amount": 0,
                        "base_discount_invoiced": 0,
                        "base_discount_tax_compensation_amount": 0,
                        "base_discount_tax_compensation_invoiced": 0,
                        "base_original_price": 29,
                        "base_price": 29,
                        "base_price_incl_tax": 31.39,
                        "base_row_invoiced": 29,
                        "base_row_total": 29,
                        "base_row_total_incl_tax": 31.39,
                        "base_tax_amount": 2.39,
                        "base_tax_invoiced": 2.39,
                        "created_at": "2019-01-30 18:48:23",
                        "discount_amount": 0,
                        "discount_invoiced": 0,
                        "discount_percent": 0,
                        "free_shipping": 0,
                        "discount_tax_compensation_amount": 0,
                        "discount_tax_compensation_invoiced": 0,
                        "is_qty_decimal": 0,
                        "item_id": 1,
                        "name": "Iris Workout Top",
                        "no_discount": 0,
                        "order_id": 1,
                        "original_price": 29,
                        "price": 29,
                        "price_incl_tax": 31.39,
                        "product_id": 1434,
                        "product_type": "configurable",
                        "qty_canceled": 0,
                        "qty_invoiced": 1,
                        "qty_ordered": 1,
                        "qty_refunded": 0,
                        "qty_shipped": 1,
                        "row_invoiced": 29,
                        "row_total": 29,
                        "row_total_incl_tax": 31.39,
                        "row_weight": 1,
                        "sku": "WS03-XS-Red",
                        "store_id": 1,
                        "tax_amount": 2.39,
                        "tax_invoiced": 2.39,
                        "tax_percent": 8.25,
                        "updated_at": "2019-01-30 18:48:33",
                        "weight": 1
                    }
                ],
                "base_currency_code": "USD",
                "base_discount_amount": 0,
                "base_discount_invoiced": 0,
                "base_grand_total": 36.39,
                "base_discount_tax_compensation_amount": 0,
                "base_discount_tax_compensation_invoiced": 0,
                "base_shipping_amount": 5,
                "base_shipping_discount_amount": 0,
                "base_shipping_discount_tax_compensation_amnt": 0,
                "base_shipping_incl_tax": 5,
                "base_shipping_invoiced": 5,
                "base_shipping_tax_amount": 0,
                "base_subtotal": 29,
                "base_subtotal_incl_tax": 31.39,
                "base_subtotal_invoiced": 29,
                "base_tax_amount": 2.39,
                "base_tax_invoiced": 2.39,
                "base_total_due": 0,
                "base_total_invoiced": 36.39,
                "base_total_invoiced_cost": 0,
                "base_total_paid": 36.39,
                "base_to_global_rate": 1,
                "base_to_order_rate": 1,
                "billing_address_id": 2,
                "created_at": "2019-01-30 18:48:23",
                "customer_dob": "1973-12-15 00:00:00",
                "customer_email": "roni_cost@example.com",
                "customer_firstname": "Veronica",
                "customer_gender": 2,
                "customer_group_id": 1,
                "customer_id": 1,
                "customer_is_guest": 0,
                "customer_lastname": "Costello",
                "customer_note_notify": 1,
                "discount_amount": 0,
                "discount_invoiced": 0,
                "entity_id": 1,
                "global_currency_code": "USD",
                "grand_total": 36.39,
                "discount_tax_compensation_amount": 0,
                "discount_tax_compensation_invoiced": 0,
                "increment_id": "000000001",
                "is_virtual": 0,
                "order_currency_code": "USD",
                "protect_code": "c558ead18ce560f7a84688621b0bde44",
                "quote_id": 1,
                "shipping_amount": 5,
                "shipping_description": "Flat Rate - Fixed",
                "shipping_discount_amount": 0,
                "shipping_discount_tax_compensation_amount": 0,
                "shipping_incl_tax": 5,
                "shipping_invoiced": 5,
                "shipping_tax_amount": 0,
                "state": "processing",
                "status": "processing",
                "store_currency_code": "USD",
                "store_id": 1,
                "store_name": "Main Website\nMain Website Store\n",
                "store_to_base_rate": 0,
                "store_to_order_rate": 0,
                "subtotal": 29,
                "subtotal_incl_tax": 31.39,
                "subtotal_invoiced": 29,
                "tax_amount": 2.39,
                "tax_invoiced": 2.39,
                "total_due": 0,
                "total_invoiced": 36.39,
                "total_item_count": 1,
                "total_paid": 36.39,
                "total_qty_ordered": 1,
                "updated_at": "2019-01-30 18:48:33",
                "weight": 1,
                "billing_address": {
                    "address_type": "billing",
                    "city": "Calder",
                    "country_id": "US",
                    "customer_address_id": 1,
                    "email": "roni_cost@example.com",
                    "entity_id": 2,
                    "firstname": "Veronica",
                    "lastname": "Costello",
                    "parent_id": 1,
                    "postcode": "49628-7978",
                    "region": "Michigan",
                    "region_code": "MI",
                    "region_id": 33,
                    "street": [
                        "6146 Honey Bluff Parkway"
                    ],
                    "telephone": "(555) 229-3326"
                },
                "payment": {
                    "account_status": null,
                    "additional_information": [
                        "Check / Money order"
                    ],
                    "amount_ordered": 36.39,
                    "amount_paid": 36.39,
                    "base_amount_ordered": 36.39,
                    "base_amount_paid": 36.39,
                    "base_shipping_amount": 5,
                    "base_shipping_captured": 5,
                    "cc_last4": null,
                    "entity_id": 1,
                    "method": "checkmo",
                    "parent_id": 1,
                    "shipping_amount": 5,
                    "shipping_captured": 5
                },
                "status_histories": [],
                "extension_attributes": {
                    "shipping_assignments": [
                        {
                            "shipping": {
                                "address": {
                                    "address_type": "shipping",
                                    "city": "Calder",
                                    "country_id": "US",
                                    "customer_address_id": 1,
                                    "email": "roni_cost@example.com",
                                    "entity_id": 1,
                                    "firstname": "Veronica",
                                    "lastname": "Costello",
                                    "parent_id": 1,
                                    "postcode": "49628-7978",
                                    "region": "Michigan",
                                    "region_code": "MI",
                                    "region_id": 33,
                                    "street": [
                                        "6146 Honey Bluff Parkway"
                                    ],
                                    "telephone": "(555) 229-3326"
                                },
                                "method": "flatrate_flatrate",
                                "total": {
                                    "base_shipping_amount": 5,
                                    "base_shipping_discount_amount": 0,
                                    "base_shipping_discount_tax_compensation_amnt": 0,
                                    "base_shipping_incl_tax": 5,
                                    "base_shipping_invoiced": 5,
                                    "base_shipping_tax_amount": 0,
                                    "shipping_amount": 5,
                                    "shipping_discount_amount": 0,
                                    "shipping_discount_tax_compensation_amount": 0,
                                    "shipping_incl_tax": 5,
                                    "shipping_invoiced": 5,
                                    "shipping_tax_amount": 0
                                }
                            },
                            "items": [
                                {
                                    "amount_refunded": 0,
                                    "base_amount_refunded": 0,
                                    "base_discount_amount": 0,
                                    "base_discount_invoiced": 0,
                                    "base_discount_tax_compensation_amount": 0,
                                    "base_discount_tax_compensation_invoiced": 0,
                                    "base_original_price": 29,
                                    "base_price": 29,
                                    "base_price_incl_tax": 31.39,
                                    "base_row_invoiced": 29,
                                    "base_row_total": 29,
                                    "base_row_total_incl_tax": 31.39,
                                    "base_tax_amount": 2.39,
                                    "base_tax_invoiced": 2.39,
                                    "created_at": "2019-01-30 18:48:23",
                                    "discount_amount": 0,
                                    "discount_invoiced": 0,
                                    "discount_percent": 0,
                                    "free_shipping": 0,
                                    "discount_tax_compensation_amount": 0,
                                    "discount_tax_compensation_invoiced": 0,
                                    "is_qty_decimal": 0,
                                    "item_id": 1,
                                    "name": "Iris Workout Top",
                                    "no_discount": 0,
                                    "order_id": 1,
                                    "original_price": 29,
                                    "price": 29,
                                    "price_incl_tax": 31.39,
                                    "product_id": 1434,
                                    "product_type": "configurable",
                                    "qty_canceled": 0,
                                    "qty_invoiced": 1,
                                    "qty_ordered": 1,
                                    "qty_refunded": 0,
                                    "qty_shipped": 1,
                                    "row_invoiced": 29,
                                    "row_total": 29,
                                    "row_total_incl_tax": 31.39,
                                    "row_weight": 1,
                                    "sku": "WS03-XS-Red",
                                    "store_id": 1,
                                    "tax_amount": 2.39,
                                    "tax_invoiced": 2.39,
                                    "tax_percent": 8.25,
                                    "updated_at": "2019-01-30 18:48:33",
                                    "weight": 1
                                }
                            ]
                        }
                    ]
                }
            }
        ]
    }