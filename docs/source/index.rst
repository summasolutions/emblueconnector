emBlue Magento 2 Connector
==========================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation.rst
   configuration.rst
   api.rst
   testing.rst

This is modules allows integration between *Magento 2* and emBlue *Email Marketing*