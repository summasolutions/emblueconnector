#######
Testing
#######

*******
Swagger
*******


#. In order to test all the enpoinds the easiest way is to use Magento buildin Swagger. To access it go to the URL **http://magento.localhost/index.php/swagger#/**.

#. Once there an admin token needs to be generated. In order to do so, scroll down to the endpoint **integrationAdminTokenServiceV1**.
    
.. image:: _images/swagger-1.jpg


3. Copy the generated token

.. image:: _images/swagger-2.jpg

4. Place the token in the header of Swagger and click **Apply**

.. image:: _images/swagger-3.jpg

5. Once completed then all the emBlue endpoints should be available to test using Swagger

.. image:: _images/swagger-4.jpg