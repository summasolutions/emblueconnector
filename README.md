# emBlue Magento 2 Connector

This module allows to connect emBlue with Magento 2

For the full documentation please check  [here](https://emblue-magento-2-connector.readthedocs.io/en/latest/)