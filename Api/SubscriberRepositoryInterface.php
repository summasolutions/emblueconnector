<?php

namespace Emblue\Connector\Api;

interface SubscriberRepositoryInterface
{

    /**
     * Retrieve subscribers which match a specified criteria.
     *
     * @param int $subscriberStatus
     * @param int $fromSubscriberId
     * @param int $curPage
     * @param int $pageSize
     * @return \Emblue\Connector\Api\Data\SubscriberSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getSubscribers($subscriberStatus = 1, $fromSubscriberId = 1, $curPage = 1, $pageSize = 100);
}
