<?php
/**
 * Class CatalogRepositoryInterface
 *
 * @author   Facundo Capua <facundocapua@gmail.com>
 * @license  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Emblue\Connector\Api;

interface CatalogRepositoryInterface
{
    /**
     * Get product list
     *
     * @param string $productName
     * @param integer $curPage
     * @param integer $pageSize
     * @return \Emblue\Connector\Api\Data\ProductSearchResultsInterface
     */
    public function getProducts($productName = '', $curPage = 1, $pageSize = 100);

    /**
     * Get related product list
     *
     * @param integer $productId
     * @return \Emblue\Connector\Api\Data\ProductSearchResultsInterface
     */
    public function getRelatedProducts($productId);
}
