<?php

namespace Emblue\Connector\Api;

interface SalesRepositoryInterface
{
    /**
     * Retrieve carts which match a specified criteria.
     *
     * @param string $customerEmail
     * @param int $curPage
     * @param int $pageSize
     * @param string $sinceCreatedAt
     * @param string $toCreatedAt
     * @param string $sinceUpdatedAt
     * @param string $toUpdatedAt
     * @return \Emblue\Connector\Api\Data\CartSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCarts(
        $customerEmail = '',
        $curPage = 1,
        $pageSize = 100,
        $sinceCreatedAt = '',
        $toCreatedAt = '',
        $sinceUpdatedAt = '',
        $toUpdatedAt = ''
    );

    /**
     * Retrieve carts which match a specified criteria.
     *
     * @param int $curPage
     * @param int $pageSize
     * @param string $customerEmail
     * @param string $sinceCreatedAt
     * @param string $toCreatedAt
     * @param string $sinceUpdatedAt
     * @param string $toUpdatedAt
     * @param string $status
     * @return \Emblue\Connector\Api\Data\OrderSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getOrders(
        $curPage = 1,
        $pageSize = 100,
        $customerEmail = '',
        $sinceCreatedAt = '',
        $toCreatedAt = '',
        $sinceUpdatedAt = '',
        $toUpdatedAt = '',
        $status = 'pending'
    );
}
