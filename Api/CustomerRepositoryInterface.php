<?php
namespace Emblue\Connector\Api;

interface CustomerRepositoryInterface
{

    /**
     * Retrieve customers which match a specified criteria.
     *
     * @param string $dateFrom
     * @param int $curPage
     * @param int $pageSize
     * @param bool $onlyNew
     * @return \Emblue\Connector\Api\Data\CustomerSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCustomers($dateFrom = '', $curPage = 1, $pageSize = 1000, $onlyNew = false);
}
