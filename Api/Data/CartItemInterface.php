<?php

namespace Emblue\Connector\Api\Data;

interface CartItemInterface extends \Magento\Quote\Api\Data\CartItemInterface
{
    /**
     * Gets the product object
     *
     * @return \Emblue\Connector\Api\Data\ProductInterface
     */
    public function getProduct();
}
