<?php
namespace Emblue\Connector\Api\Data;

interface ProductInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{
    /**
     * Product id
     *
     * @return int|null
     */
    public function getId();

    /**
     * Product sku
     *
     * @return string
     */
    public function getSku();

    /**
     * Product name
     *
     * @return string|null
     */
    public function getName();

    /**
     * Product price
     *
     * @return float|null
     */
    public function getPrice();

    /**
     * Product status
     *
     * @return int|null
     */
    public function getStatus();

    /**
     * Product formatted price
     *
     * @return string|null
     */
    public function getFormattedPrice();

    /**
     * Product final price
     *
     * @return float|null
     */
    public function getFinalPrice();

    /**
     * Product url path price
     *
     * @return string|null
     */
    public function getProductUrl();

    /**
     * Product url price
     *
     * @return string|null
     */
    public function getUrl();

    /**
     * Product special price
     *
     * @return float|null
     */
    public function getSpecialPrice();

    /**
     * Product special price date from
     *
     * @return string|null
     */
    public function getSpecialFromDate();

    /**
     * Product special price date to
     *
     * @return string|null
     */
    public function getSpecialToDate();

    /**
     * Product url key
     *
     * @return string|null
     */
    public function getUrlKey();

    /**
     * Product website IDs
     *
     * @return int[]|null
     */
    public function getWebsiteIds();

    /**
     * Product category
     *
     * @return \Magento\Catalog\Api\Data\CategoryInterface|null
     */
    public function getCategory();

    /**
     * Product description
     *
     * @return string|null
     */
    public function getDescription();

    /**
     * Get media gallery entries
     *
     * @return \Emblue\Connector\Api\Data\MediaGalleryEntryCollectionInterface|null
     */
    public function getMediaGalleryImages();

    /**
     * Product type id
     *
     * @return string|null
     */
    public function getTypeId();
}
