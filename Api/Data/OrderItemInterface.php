<?php

namespace Emblue\Connector\Api\Data;

interface OrderItemInterface extends \Magento\Sales\Api\Data\OrderItemInterface
{
    /**
     * Gets the product object
     *
     * @return \Emblue\Connector\Api\Data\ProductInterface
     */
    public function getProduct();
}
