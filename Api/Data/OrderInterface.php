<?php

namespace Emblue\Connector\Api\Data;

interface OrderInterface extends \Magento\Sales\Api\Data\OrderInterface
{
    /**
     * Gets items for the order.
     *
     * @return \Emblue\Connector\Api\Data\OrderItemInterface[] Array of items.
     */
    public function getItems();
}
