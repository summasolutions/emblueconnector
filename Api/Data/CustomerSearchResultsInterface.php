<?php

namespace Emblue\Connector\Api\Data;

interface CustomerSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get customers list.
     *
     * @return \Emblue\Connector\Api\Data\CustomerInterface[]
     */
    public function getItems();

    /**
     * Set customers list.
     *
     * @param \Emblue\Connector\Api\Data\CustomerInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
