<?php

namespace Emblue\Connector\Api\Data;

interface CartInterface extends \Magento\Quote\Api\Data\CartInterface
{
    /**
     * Gets items for the order.
     *
     * @return \Emblue\Connector\Api\Data\CartItemInterface[] Array of items.
     */
    public function getItems();
}
