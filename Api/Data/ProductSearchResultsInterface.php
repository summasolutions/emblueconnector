<?php
namespace Emblue\Connector\Api\Data;

interface ProductSearchResultsInterface
{
    /**
     * Get attributes list.
     *
     * @return \Emblue\Connector\Api\Data\ProductInterface[]
     */
    public function getItems();

    /**
     * Set attributes list.
     *
     * @param \Emblue\Connector\Api\Data\ProductInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
