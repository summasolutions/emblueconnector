<?php

namespace Emblue\Connector\Api\Data;

interface CartSearchResultInterface
{
    /**
     * Get customers list.
     *
     * @return \Emblue\Connector\Api\Data\CartInterface[]
     */
    public function getItems();

    /**
     * Set customers list.
     *
     * @param \Emblue\Connector\Api\Data\CartInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
