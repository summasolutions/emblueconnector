<?php
/**
 * Class MediaGalleryEntryCollectionInterface
 *
 * @author   Facundo Capua <facundocapua@gmail.com>
 * @license  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Emblue\Connector\Api\Data;

interface MediaGalleryEntryCollectionInterface
{
    /**
     * @return \Emblue\Connector\Api\Data\MediaGalleryEntryInterface[]
     */
    public function getItems();
}
