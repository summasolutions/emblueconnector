<?php

namespace Emblue\Connector\Api\Data;

interface SubscriberSearchResultsInterface
{
    /**
     * Get customers list.
     *
     * @return \Emblue\Connector\Api\Data\SubscriberInterface[]
     */
    public function getItems();

    /**
     * Set customers list.
     *
     * @param \Emblue\Connector\Api\Data\SubscriberInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
