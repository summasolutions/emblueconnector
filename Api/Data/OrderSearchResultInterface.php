<?php

namespace Emblue\Connector\Api\Data;

interface OrderSearchResultInterface
{
    /**
     * Get customers list.
     *
     * @return \Emblue\Connector\Api\Data\OrderInterface[]
     */
    public function getItems();

    /**
     * Set customers list.
     *
     * @param \Emblue\Connector\Api\Data\OrderInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
