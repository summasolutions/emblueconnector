<?php

namespace Emblue\Connector\Api\Data;

interface SubscriberInterface
{
    /**
     * Subscriber Id
     *
     * @return int|null
     */
    public function getId();

    /**
     * Subscriber Email
     *
     * @return string|null
     */
    public function getEmail();

    /**
     * Subscriber Status
     *
     * @return int|null
     */
    public function getStatus();

    /**
     * Subscriber Status
     *
     * @return int|null
     */
    public function getCustomerId();
}
