<?php
namespace Emblue\Connector\Api\Data;

interface MediaGalleryEntryInterface
{

    /**
     * Returns Image entry url
     *
     * @return string|null
     */
    public function getUrl();
}
